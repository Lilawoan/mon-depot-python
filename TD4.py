import requests
from bs4 import BeautifulSoup
from collections import defaultdict
# import pandas as pd


# UNI = requests.get("https://stackoverflow.com/questions/tagged/beautifulsoup")


# soup = BeautifulSoup(UNI.text, "lxml")
# quest = soup.find_all("a", {"class": "question-hyperlink"}, limit=10)
# vote = soup.find_all("span", {"class": "vote-count-post"}, limit=10)
# answer = soup.select("div.status > strong", limit=10)
# for a in range(10):
#    print(quest[a].get_text() + " " +
#          vote[a].get_text() + " " + answer[a].get_text())


# payload = {'kesubmit-form:': '', 'zone-item-id': 'zoneItem://c8e50408-29b9-4eb9-9b3f-1c209fb2d75b', 'catalog': 'catalogue-2015-2016',
#           'title': '', 'textfield': '', 'degree': 'DP', 'orgUnit:orgunitContent': '//9ee7f4af-c6e8-406a-8292-dea5cdf178c1', 'place': '45000'}
# r = requests.post(
#    "http://formation.univ-orleans.fr/fr/formation/rechercher-une-formation.html#nav", data=payload)
# print(r.text)

def get_definition(x):
    URL = 'http://services.aonaware.com/DictService/Default.aspx?action=define&dict=wn&query={0}'.format(
        x)
    html = requests.get(URL).text
    soup = BeautifulSoup(html, "lxml")
    defi = soup.select("a > pre")
    elt = defi.pop()
    return elt.get_text()
    #  return defi


def defpayload(x):
    payload = {'action': 'define', 'dict': 'wn', 'query': x}
    URL = 'http://services.aonaware.com/DictService/Default.aspx'
    html = requests.get(URL, params=payload).text
    soup = BeautifulSoup(html, "lxml")
    defi = soup.select("a > pre")
    elt = defi.pop()
    return elt.get_text()


print(get_definition("breakfast"))
print(defpayload("breakfast"))

lines = []
fichierSortie = open('definitions.txt', 'w+')
with open('vocabulary.txt') as f:
    lines = f.readlines()
    for line in lines:
        fichierSortie.write(defpayload(line) + '\n')
        # print(defpayload(line))
